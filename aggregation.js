// [SECTION] MongoDB Aggregation
/*
    - Used to generate, manipulate and perform operations to create filtered results that help us analyze the data.
*/

    // Using the aggreagate method.
    /*
       - The "$match" is used to pass the document that meet the sopecified condition(s) to the next stage/ aggregation process.
       - Syntax:
            - {$match: {field: value}};
        - The "$group" is used to group elements together and field-value pairs. the data from the grouped elements.

        - Syntax:
            - {$group: {_id: "value", fieldResults: "valueResult"}};
        - Using both "$match" and "$group" along with aggregation will find for products that are on sale and will group the total amount of stock for all suppliers found.
        - Syntax
            - db.collectionName.aggregate([{$match:{fieldA:valueA}}, {$group: {_id: "value", fieldResults: "valueResult"}}]);

    */

    db.fruits.aggregate([
        {$match: {onSale: true}},
        {$group: {_id: "$supplier_id", total: { $sum: "$stock"}
            }}
        ]);

    // Field Projecton with aggregation
    /*
        - The "$project" can be used when aggragating data to include/exclude fields from the returned results.

        -Syntax:
            {$project: {field: 1/0}}

    */

    db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$supplier_id", total: { $sum: "$stock"}
        }},
        {$project: {_id:0}}
    ]);

    // Sorting aggregated results
    /*
        - The "$sort" can be used to change the order of the aggregated result
        - Syntax:
            {$sort: {field:1/-1}}
            1 -> lowest to highest
            -1 -> highest to lowest
    */

    db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$supplier_id", total: { $sum: "$stock"}
        }},
        {$sort: {total:1}}
    ]);

    // Aggregating results based ion array fields

        /*
            - The "$unwind" deconstructs an array field from the collection/field with an array value to output a result.
            - Syntax:
                - {$unwind: field}

        */

        db.fruits.aggregate([
                {$unwind: "$origin"},              //+=
                {$group: {_id: "$origin", kinds: {$sum: 1}
                    }},
            ]);

        // [SECTION] Other aggreagate stages

        // $count all yellow fruits
        db.fruits.aggregate([
                {$match: {color: "Yellow"}},
                {$count: "Yellow Fruits"}
            ]);
        //$avg gets the average value of stocks
         db.fruits.aggregate([
                {$match: {color: "Yellow"}},
                {$group: {_id: "$color", yellow_fruits_stock:{$avg: "$stock"}}}
            ]);
         // $min & $max
           db.fruits.aggregate([
                {$match: {color: "Yellow"}},
                {$group: {_id: "$color", yellow_fruits_stock:{$min: "$stock"}}}
            ]);

             db.fruits.aggregate([
                {$match: {color: "Yellow"}},
                {$group: {_id: "$color", yellow_fruits_stock:{$max: "$stock"}}}
            ]);